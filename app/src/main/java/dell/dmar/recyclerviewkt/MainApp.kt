package dell.dmar.recyclerviewkt

import android.app.Application
import dell.dmar.recyclerviewkt.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.module.Module

class MainApp : Application() {

    override fun onCreate() {
        super.onCreate()

        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidLogger()
            androidContext(this@MainApp)
            modules(provideModules())
        }
    }

    private fun provideModules(): List<Module> =
        listOf(
            appModule
        )
}
// wyj na urz 15 19 22