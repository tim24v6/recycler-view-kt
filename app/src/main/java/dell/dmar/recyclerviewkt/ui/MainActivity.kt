package dell.dmar.recyclerviewkt.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import dell.dmar.recyclerviewkt.R
import dell.dmar.recyclerviewkt.model.ItemsViewModel
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val linearLayoutManager: LinearLayoutManager by inject()
    private val gridLayoutManager: StaggeredGridLayoutManager by inject()
    private val itemDecoration: DividerItemDecoration by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initRecyclerView()
    }

    fun initRecyclerView() {
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
//        recyclerView.layoutManager = LinearLayoutManager(this) pominiety bo dodany koin
//        recyclerView.setitemAnimator(DefaultItemAnimator()) pominiety dla uproszczenia
        val data = ArrayList<ItemsViewModel>()

        for (i in 1..20) {
            data.add(ItemsViewModel(R.drawable.ic_baseline_folder_open_24, "Item: $i"))
        }
        val adapter = CustomAdapter(data)

        // setting the Adapter with the recyclerView
        recyclerView.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return super.onOptionsItemSelected(item)
    }
}